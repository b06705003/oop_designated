package com.example.sad_poc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

// used for recyclerView
public class WordsAdapter extends RecyclerView.Adapter<WordsAdapter.WordViewHolder> {

    private List<Word> wordList;

    public WordsAdapter(List<Word> words) { this.wordList = words; }

    public class WordViewHolder extends RecyclerView.ViewHolder {

        public TextView englishTv;
        public Button chineseBtn;
        public CheckBox favoriteCb;

        public WordViewHolder(@NonNull View itemView) {
            super(itemView);

            // access views
            englishTv = itemView.findViewById(R.id.word_english);
            chineseBtn = itemView.findViewById(R.id.word_chinese);
            favoriteCb = itemView.findViewById(R.id.favorite_checkbox);
        }
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View wordView = inflater.inflate(R.layout.word_item, parent, false);

        // Return a new holder instance
        WordViewHolder viewHolder = new WordViewHolder(wordView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {

        // Get the data model based on position
        final Word word = wordList.get(position);

        // Set item views based on your views and data model
        TextView textView = holder.englishTv;
        textView.setText(word.getEnglish());
        final Button button = holder.chineseBtn;
        button.setText("中文");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (button.getText().toString().equals("中文")) {
                    button.setText(word.getChinese());
                }
                else {
                    button.setText("中文");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return wordList.size();
    }

}
