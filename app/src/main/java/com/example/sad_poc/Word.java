package com.example.sad_poc;

import java.util.ArrayList;
import java.util.List;

public class Word {
    private String english;
    private String chinese;
    private String sentence;
    private String synonym;
    private String antonym;
    private boolean memorized;
    private boolean favorite;

    public static List<Word> createWordList() { // get list of words
        List<Word> wordList = new ArrayList<>();

        wordList.add(new Word("elucidate", "闡明，說明", "The reasons for the change in weather conditions have been elucidated by several scientists.", "clarify, clear, construe", "obscure"));
        wordList.add(new Word("lucid", "清澈的，透明的，透徹的", "She gave a clear and lucid account of her plans for the company's future.", "beaming, bedazzling, bright", "dim, dull, lackluster"));
        wordList.add(new Word("erudite", "博學的", "He's the author of an erudite book on Scottish history.", "educated, knowledgeable, learned", "benighted, dark, ignorant"));
        wordList.add(new Word("rudimentary", "基本的，初步的", "They were carpenters making chairs, beds, and other rudimentary pieces of furniture for the locals.", "crude, low, primitive, rude", "advanced, developed, evolved"));
        wordList.add(new Word("rigid", "嚴格的，剛硬的", "We were disappointed that they insisted on such a rigid interpretation of the rules.", "brassbound, exacting, inflexible", "flexible, lax, loose, slack"));
        wordList.add(new Word("spontaneous", "自發的，自然的", "His jokes seemed spontaneous, but were in fact carefully prepared beforehand.", "automatic, instinctive, instinctual, ", "scripted, evoked"));
        wordList.add(new Word("boorish", "粗野的，粗魯的", "The commercial culture will tolerate multiple divorces, trip to rehab, and all sorts of boorish behavior.", "churlish, classless, cloddish", "refined"));
        wordList.add(new Word("opportune", "恰好的，適宜的", "This seems to be an opportune moment for reviving our development plan.", "seasonable, timely", "unseasonable, untimely"));
        wordList.add(new Word("explicable", "可解釋的，可辨明的", "Failures in this process, however, are explicable in realist terms as the protection of national sovereignty.", "answerable, explainable, resolvable", "insoluble, unexplainable"));
        wordList.add(new Word("erroneous", "錯誤的，不正確的", "Instead, they appeared on occasion to recompute the phonetic elements, producing a new erroneous version.", "false, inaccurate, incorrect", "accurate, correct, errorless"));
        return wordList;
    }

    public Word() {}

    // constructor
    public Word(String english, String chinese, String sentence, String synonym, String antonym) {
        this.english = english;
        this.chinese = chinese;
        this.sentence = sentence;
        this.synonym = synonym;
        this.antonym = antonym;
        this.memorized = false;
        this.favorite = false;
    }

    // get question 1: give sentence, choose chinese meaning
    public Question getQuestion1() {
        List<String> wrongs = new ArrayList<>();
        for (String option: Question.getThreeChineseOptions())
            wrongs.add(option);

        return Question.generateQuestion(sentence, chinese, wrongs);
    }

    // get question 2: give chinese, choose english word
    public Question getQuestion2() {
        List<String> wrongs = new ArrayList<>();
        for (String option: Question.getThreeEnglishOptions())
            wrongs.add(option);

        return Question.generateQuestion(chinese, english, wrongs);
    }

    // get question 3: give synonym and antonym, choose english word
    public Question getQuestion3() {
        List<String> wrongs = new ArrayList<>();
        for (String option: Question.getThreeEnglishOptions())
            wrongs.add(option);

        return Question.generateQuestion(synAntToString(), english, wrongs);
    }

    private String synAntToString() {
        return "同義: " + synonym + "\n反義: " + antonym;
    }

    @Override
    public String toString() {
        return "Word{" +
                "english='" + english + '\'' +
                ", chinese='" + chinese + '\'' +
                ", sentence='" + sentence + '\'' +
                ", synonym='" + synonym + '\'' +
                ", antonym='" + antonym + '\'' +
                '}';
    }

    public String getEnglish() {
        return english;
    }

    public String getChinese() {
        return chinese;
    }
}
