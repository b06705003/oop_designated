package com.example.sad_poc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Question {
    private String question;
    private List<String> options;
    private int answer;

    // data for wrong chinese options
    public static List<String> WRONG_CHINESE_LIST = Arrays.asList(
            "欺騙，哄騙", "逃避，躲避", "辱罵，謾罵，痛斥",
            "奸詐的", "不完全的，粗略的", "滑稽的，荒唐可笑的",
            "欺騙的，迷人的", "多孔的，能被穿透的", "有密切關係的，貼切的",
            "無破綻的，牢不可破的", "過時的，陳舊的，老式的", "縹緲的，超凡的",
            "拖延的", "奸詐的", "費解的，旋繞的",
            "不完全的，粗略的", "錯綜複雜的", "健談的，口若懸河的",
            "羞愧地", "滑稽的，荒唐可笑的", "堅決的，堅持的",
            "謹慎地", "繁重的，麻煩的", "難以置信的",
            "變化無常的，任性的", "不穩定的，古怪的", "惡意的，心懷叵測的",
            "無破綻的，牢不可破的", "有密切關係的，貼切的", "錯綜複雜的");

    public static List<String> getThreeChineseOptions() { // used for generate three wrong chinese options
        Collections.shuffle(WRONG_CHINESE_LIST);
        List<String> options = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            options.add(WRONG_CHINESE_LIST.get(i));
        }
        return options;
    }

    // data for wrong english options
    public static final List<String> WRONG_ENGLISH_LIST = Arrays.asList(
            "abnegate", "accolade", "brazen",
            "buoyant", "callous", "cathartic",
            "convection", "despicable", "dote",
            "efface", "epitome", "fleeting",
            "futile", "gloat", "harbinger",
            "havoc", "impromptu", "intrusive",
            "lugubrious", "mediocre", "morbid",
            "nugatory", "opulence", "panache",
            "prig", "quibble", "referent",
            "seraphic", "simpleton", "stifle",
            "supersede", "tenacious", "tussle",
            "unctuous", "unwitting", "vindicate",
            "welter", "urbanity", "unkempt",
            "tyranny", "tractable", "terse",
            "subdue", "slumber", "shore",
            "rosy", "retrenchment", "reciprocity",
            "rampant", "prophylactic", "pastiche");

    public static List<String> getThreeEnglishOptions() { // used for generate three wrong english options
        Collections.shuffle(WRONG_ENGLISH_LIST);
        List<String> options = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            options.add(WRONG_ENGLISH_LIST.get(i));
        }
        return options;
    }

    // generate a question
    public static Question generateQuestion(String question, String answer, List<String> wrongs) {
        int correctIndex = new Random().nextInt(4);

        Collections.shuffle(wrongs);
        wrongs.add(correctIndex, answer);

        return new Question(question, wrongs, correctIndex);
    }

    // constructors
    public Question() {}

    public Question(String question, List<String> options, int answer) {
        this.question = question;
        this.options = options;
        this.answer = answer;
    }

    // getters and setters
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
